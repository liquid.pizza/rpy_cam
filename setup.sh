# Install `poetry`
curl -sSL https://install.python-poetry.org | python3 -

# Add `poetry` to the `PATH`
export PATH="/home/jan/.local/bin:$PATH"

# Add the export command to `.bashrc`
echo 'export PATH="/home/jan/.local/bin:$PATH"' >> ~/.bashrc

# Install poetry packages
poetry install
