from fastapi import FastAPI
from typing import Annotated
from fastui import FastUI, AnyComponent, prebuilt_html, components as c
from fastui.events import GoToEvent, BackEvent, PageEvent
from fastui.forms import fastui_form
from fastapi.responses import HTMLResponse, FileResponse
from contextlib import asynccontextmanager

from config import Config
from rpy_cam import RpyCam

print("load app.py")

@asynccontextmanager
async def lifespan(app: FastAPI):
    global rpy_cam
    rpy_cam = RpyCam()

    yield

app = FastAPI(lifespan=lifespan)

@app.get("/api/", response_model=FastUI, response_model_exclude_none=True)
def set_config() -> list[AnyComponent]:
    """
    Show a table of four users, `/api` is the endpoint the frontend will connect to
    when a user visits `/` to fetch components to render.
    """
    return [
        c.Page(  # Page provides a basic container for components
            components=[
                c.Heading(text='Welcome', level=1,),  # renders `<h2>Users</h2>`
                # c.Button(
                #     text='Show info',
                #     on_click=PageEvent(name='static-modal'),
                #     class_name="btn btn-info fixed-top",
                # ),
                # c.Modal(
                #     title="Test Modal",
                #     body=[
                #         c.Text("Hello!")
                #     ],
                #     open_trigger=PageEvent(name='static-modal'),
                # ),
                # c.ModelForm(
                #     model=Config,
                #     submit_url="/api/config",
                #     display_mode="default",
                # ),
                c.Link(
                    components=[c.Button(text="Config")],
                    on_click=GoToEvent(url="/config")
                ),
                c.Link(
                    components=[c.Button(text="Preview")],
                    on_click=GoToEvent(url="/preview")
                ),
            ]
        ),
    ]

@app.get("/api/hello", response_model=FastUI, response_model_exclude_none=True)
def hello() -> list[AnyComponent]:
    """
    Show a table of four users, `/api` is the endpoint the frontend will connect to
    when a user visits `/` to fetch components to render.
    """
    return [
        c.Page(  # Page provides a basic container for components
            components=[
                c.Heading(text='Welcome', level=1,),
            ]
        ),
    ]

@app.get("/api/preview", response_model=FastUI, response_model_exclude_none=True)
def get_preview() -> list[AnyComponent]:
    """
    Show a table of four users, `/api` is the endpoint the frontend will connect to
    when a user visits `/` to fetch components to render.
    """
    import os
    global rpy_cam
    path = rpy_cam.preview()
    path = f"/show?path={path}"
    return [
        c.Page(  # Page provides a basic container for components
            components=[
                c.Heading(text='Preview', level=1,),
                c.Image(
                    src=path,
                ),
            ]
        ),
    ]

@app.get("/hello")
async def api_hello(name:str = "World"):
    return {"msg": f"Hello {name}!"}

@app.get("/show")
async def api_show(path:str = RpyCam.LATEST_PATH):
    return FileResponse(path)

@app.get("/api/config", response_model=FastUI, response_model_exclude_none=True)
def get_config() -> list[AnyComponent]:
    """
    Shows the config
    """
    return [
        c.Page(  # Page provides a basic container for components
            components=[
                c.Heading(text='Camera config', level=1,), 
                c.ModelForm(
                    model=Config,
                    submit_url="/api/config",
                    display_mode="default",
                ),
            ]
        ),
    ]

@app.post("/api/config", response_model=FastUI, response_model_exclude_none=True)
def post_config(form: Annotated[Config, fastui_form(Config)]):
    global rpy_cam
    rpy_cam.conf = form
    print(rpy_cam.conf)
    return [
        c.FireEvent(event=GoToEvent(url='/run')),
    ]

@app.get("/api/run", response_model=FastUI, response_model_exclude_none=True)
def run(run: bool = True) -> list[AnyComponent]:
    """
    Show a table of four users, `/api` is the endpoint the frontend will connect to
    when a user visits `/` to fetch components to render.
    """
    global rpy_cam
    if not rpy_cam.cam_settings.is_setup:
        return [
            c.Page(  # Page provides a basic container for components
                components=[
                    c.Heading(text="Error", level=1,),
                    c.Text(text="Please ensure the camera is setup."),
                ]
            ),
        ]
    rpy_cam.run(run)
    return [
        c.Page(  # Page provides a basic container for components
            components=[
                c.Heading(text='Running...', level=1,),
            ]
        ),
    ]

@app.get('/{path:path}')
def html_landing() -> HTMLResponse:
    """Simple HTML page which serves the React app, comes last as it matches all paths."""
    return HTMLResponse(prebuilt_html(title='FastUI Demo'))