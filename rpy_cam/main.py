import uvicorn

from rpy_cam import RpyCam

print("Imports done")

rpy_cam: RpyCam

def start_ui():
    """Launched with `poetry run start` at root level"""
    uvicorn.run("app:app", host="0.0.0.0", port=8000, reload=True)

def main() -> None:
    start_ui()

if __name__ == "__main__":
    main()