from pydantic import BaseModel

"""
DATE_FORMAT_STR: str = "%y%m%d_%H%M%S"

IMG_DIR: str = "img"
IMG_PREFIX: str = ""
IMG_SUFFIX: str = "_img"
IMG_FILE_ENDING: str = "jpg"

SLEEP_TIME_S: int = 60
"""

class Config(BaseModel):
    date_format: str = "%y%m%d_%H%M%S"

    img_dir: str = "img"
    img_prefix: str = ""
    img_suffix: str = "_img"
    img_file_ending: str = "jpg"

    sleep_time_s: int = 60
