from pydantic import BaseModel

from picamera2 import Picamera2
from picamera2.request import CompletedRequest
import datetime as dt
import time

from config import Config

class CameraSettings(BaseModel):
    is_setup: bool = False
    is_running: bool = False

class RpyCam():

    PREVIEW_PATH = "preview.jpg"
    LATEST_PATH = "latest.jpg"

    def __init__(
            self,
        ):
        self.conf: Config = Config()
        self.picam: Picamera2 | None = None

        self.cam_settings: CameraSettings = CameraSettings(
            is_setup=self._setup_camera(),
        )

        self._start_threaded()

        return None

    def _start_threaded(
            self,
        ):
        import threading
        cam_thread = threading.Thread(target=self._start_timelaps)
        cam_thread.start()
        print("cam_thread started")

    def _setup_camera(
            self,
        ) -> bool:
        try:
            self.picam = Picamera2()
        except Exception as e:
            print(e)
            return False

        config = self.picam.create_still_configuration()
        self.picam.configure(config)
        self.picam.start()
        time.sleep(1)

        return True

    def _start_timelaps(
            self,
        ):
        if not self.cam_settings.is_setup:
            self.cam_settings.is_setup = self._setup_camera()

        while 1:
            if not self.cam_settings.is_running:
                continue

            r: CompletedRequest = self._take_picture()
            self._handle_pic(r)
            time.sleep(self.conf.sleep_time_s)

    def _take_picture(
        self,
    ) ->  CompletedRequest:
        r = self.picam.capture_request()
        return r

    def _handle_pic(
            self,
            r: CompletedRequest,
        ) -> None:

        conf: Config = self.conf

        date_str = dt.datetime.now().strftime(conf.date_format)
        file_name = f"{conf.img_dir}/{conf.img_prefix}{date_str}{conf.img_suffix}.{conf.img_file_ending}"
        r.save("main", file_name)
        r.save("main", RpyCam.LATEST_PATH)
        r.release()
        print(f"Saved {file_name}")

    def preview(
            self,
        ) -> str:
        r = self._take_picture()
        path: str = RpyCam.PREVIEW_PATH
        r.save("main", path)
        r.release()

        return path

    def run(
            self,
            is_running: bool = True,
        ) -> None:
        self.cam_settings.is_running = is_running