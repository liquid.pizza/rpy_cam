# https://raspberrytips.com/picamera2-raspberry-pi/
#ffmpeg -r 1 -pattern_type glob -i "img/*.jpg" -vcodec libx264 timelapse/timelapse.mp4

# https://medium.com/@sekhar.rahul/creating-a-time-lapse-video-on-the-command-line-with-ffmpeg-1a7566caf877
ffmpeg -framerate 30 -pattern_type glob -i "img/*.jpg" -s:v 1440x1080 -c:v libx264 -crf 17 -pix_fmt yuv420p output/timelaps.mp4